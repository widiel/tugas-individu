import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('textBox/OR008 - buttonElement'))

WebUI.click(findTestObject('textBox/OR009 - buttonTextBox'))

WebUI.setText(findTestObject('textBox/OR0010- fullName'), 'Widi Pamungkas')

WebUI.delay(2)

WebUI.setText(findTestObject('textBox/OR0012 - emailField'), 'widi@gmail.com')

WebUI.delay(2)

WebUI.setText(findTestObject('textBox/OR0013 - currentAddress'), 'Jakarta')

WebUI.delay(2)

WebUI.setText(findTestObject('textBox/OR0014 - permanentAddress'), 'Tasikmalaya')

WebUI.delay(2)

WebUI.click(findTestObject('textBox/OR0015 - buttonSubmit'))

WebUI.takeScreenshot()

WebUI.closeBrowser()

